class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def active_for_authentication?
  	if role == "Admin" || status == "approved"
  		super
  	end
  end

  def inactive_message
  	if status == "pending" || role != "Admin"
  		:not_approved
  	else
  		super
  	end
  end

end

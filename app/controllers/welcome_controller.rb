class WelcomeController < ApplicationController
  def index
  	@kelas = Kela.all

  end

  def show
  	if params[:role] == "mahasiswa"
  		@users = User.where(role: "mahasiswa")
  	elsif params[:role] == "dosen"
  		@users = User.where(role: "dosen")
  	else
  		@users = User.all
  	end
  end

  def edit
  	@user = User.find(params[:id])
  end

  def update
  	@user = User.find(params[:id])

  	if @user.update(user_params)
  		redirect_to welcome_show_path
  	end
  end

private
	def user_params
		params.require(:user).permit(:status)
	end

end

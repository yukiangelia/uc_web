class ApplicationController < ActionController::Base
  
  before_action :authenticate_user!

  protect_from_forgery with: :exception


  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
  	redirect_to root_url, :alert => exception.message
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |user_params|
  		user_params.permit(:email, :password, :password_confirmation, :role, :nama, :gender, :b_date, :alamat, :kota, :no_telp, :status)
    end
    devise_parameter_sanitizer.permit(:account_update) do |user_params|
      user_params.permit(:email, :role, :nama, :gender, :b_date, :alamat, :kota, :no_telp, :current_password)
    end
  
  end
end

class MahasiswaJoinKelasController < ApplicationController
	def new
		@mahasiswa_join_kela = Mahasiswa_join_kela.new
	end

	def create
	  @kela = Kela.find(params[:kela_id])
	  @mahasiswa_join_kela = @kela.mahasiswa_join_kelas.create(mhsw_join_params)
	  redirect_to kelas_path
	end

	def update
		@mahasiswa_join_kela = MahasiswaJoinKela.find(params[:id])

		if @mahasiswa_join_kela.update(mhsw_join_params)
			redirect_to kelas_path
		end
	end

	def index
		@mahasiswa_join_kelas = Mahasiswa_join_kela.all
	end


	private
	def mhsw_join_params
		params.require(:mahasiswa_join_kela).permit(:matakuliah, :mahasiswa_join, :status)
	end
end

Rails.application.routes.draw do
  
  get 'welcome/index'
  get 'welcome/show'
  get 'kelas/index'
  get 'mahasiswa/index'

  patch 'welcome/:id/edit', controller: 'welcome', action: :update

  devise_for :users

  resources :welcome
  
  resources :kelas do
  	resources :mahasiswa_join_kelas 
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'
end

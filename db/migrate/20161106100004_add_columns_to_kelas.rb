class AddColumnsToKelas < ActiveRecord::Migration[5.0]
  def change
    add_column :kelas, :nama_mk, :string
    add_column :kelas, :kode_mk, :string
  end
end

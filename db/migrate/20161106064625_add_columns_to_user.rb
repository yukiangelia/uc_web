class AddColumnsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :nama, :string
    add_column :users, :gender, :string
    add_column :users, :alamat, :string
    add_column :users, :b_date, :string
    add_column :users, :kota, :string
    add_column :users, :no_telp, :string
  end
end

class CreateMahasiswaJoinKelas < ActiveRecord::Migration[5.0]
  def change
    create_table :mahasiswa_join_kelas do |t|
      t.string :matakuliah
      t.string :mahasiswa_join

      t.timestamps
    end
  end
end
